package com.googleapp.alumno.clase04_5;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by alumno on 3/28/17.
 */

public class Configuracion extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        IntentFilter filter = new IntentFilter();
        filter.addAction(CONNECTIVITY_SERVICE);
        registerReceiver(new InternetBroadcastReceiver(),filter);
    }
}
