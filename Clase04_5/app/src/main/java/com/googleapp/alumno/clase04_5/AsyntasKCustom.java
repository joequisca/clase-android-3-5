package com.googleapp.alumno.clase04_5;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by alumno on 3/28/17.
 */

public class AsyntasKCustom  extends AsyncTask<String, Void, String>{

    Context context;
    ProgressDialog progressDialog;


    public AsyntasKCustom(Context context){
        this.context = context;
        progressDialog = new ProgressDialog(context);
    }


    @Override
    //antes de ejecutar
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog.setMessage("Cargando");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        //proceso a realizar
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        progressDialog.dismiss();
    }
}
